import { render, Component } from 'inferno';
import { HashRouter, Route, Switch } from 'inferno-router';

import { Navbar } from './components/navbar';
import { Home } from './components/home';
import { Search } from './components/search';

import './Main.css';

const container = document.getElementById('app');

class Index extends Component<any, any> {
  render() {
    return (
      <HashRouter>
        <Navbar />
        <div class="mt-3 p-0">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path={`/search/:type_/:q/:page`} component={Search} />
          </Switch>
        </div>
      </HashRouter>
    )
  }
}
render(<Index />, container);
