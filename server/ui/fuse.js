const {
  FuseBox,
  Sparky,
  EnvPlugin,
  CSSPlugin,
  WebIndexPlugin,
  QuantumPlugin
} = require('fuse-box');
// const transformInferno = require('../../dist').default
const transformInferno = require('ts-transform-inferno').default;
const transformClasscat = require('ts-transform-classcat').default;
let fuse, app;
let isProduction = false;
var setEnv = require('./set_env.js').setEnv;

Sparky.task('config', _ => {
  fuse = new FuseBox({
    homeDir: 'src',
    hash: isProduction,
    output: 'dist/$name.js',
    experimentalFeatures: true,
    cache: !isProduction,
    sourceMaps: !isProduction,
    transformers: {
      before: [transformClasscat(), transformInferno()],
    },
    plugins: [
      EnvPlugin({ NODE_ENV: isProduction ? 'production' : 'development' }),
      CSSPlugin(),
      WebIndexPlugin({
        title: 'Inferno Typescript FuseBox Example',
        template: 'src/index.html',
        path: isProduction ? "/static" : "/"
      }),
      isProduction &&
      QuantumPlugin({
        bakeApiIntoBundle: 'app',
        treeshake: true,
        uglify: true,
      }),
    ],
  });
  app = fuse.bundle('app').instructions('>index.tsx');
});
Sparky.task('clean', _ => Sparky.src('dist/').clean('dist/'));
Sparky.task('env', _ => (isProduction = true));
Sparky.task('set_env', _ => {
  setEnv();
});
Sparky.task('copy-assets', () => Sparky.src('assets/*.ico').dest('dist/'));
Sparky.task('dev', ['clean', 'set_env', 'config', 'copy-assets'], _ => {
  fuse.dev();
  app.hmr().watch();
  return fuse.run();
});
Sparky.task('prod', ['clean', 'set_env', 'env', 'config', 'copy-assets'], _ => {
  // fuse.dev({ reload: true }); // remove after demo
  return fuse.run();
});
